Last lecture we went over how to electronically create a carrier for some signal. Now we will go over how to combine a carrier and a signal, and the math behind the implementation.

$V_c$ is the carrier signal. It is a constant sinusoid, with 3 properties that do not change over the life of the carrier, defined as follows:
$$
V_c(t)=V_c\sin(\omega_ct+\theta)\\
V_c=\ amplitude\\
\omega_c=\ frequency\\
\theta=\ angle
$$
The information to be transmitted is impressed into one of these three values.

# Amplitude Modulation

This is the simplest to understand, and the simplest to implement. Simply, the two signals are added. This is demonstrated mathematically below:
$$
E_{AM}(t)=(E_c+E_i(t))*\sin(\omega_ct+\theta)\\
E_c=\ carrier\ amplitude\\
E_i(t)=\ modulating\ signal\\
\omega_ct+\theta=fixed\ frequency\ and\ phase
$$
In the case of a sinusoid modulation, $E_i(t)=E_i\sin(2\pi f_mt)$.

Visually, this looks like a high-frequency wave ($E_c)$, whose peaks and valleys are determined by a pair of sinusoids ($E_i$), as seen below. (Image cropped due to overlapping irrelevant text)

![image-20201030210803360](lecture13.assets/image-20201030210803360.png)

The modulation index is the ratio of the modulating signal to the carrier signal. In the example above, the modulation frequency is 50%, calculated as follows:
$$
m=\frac{E_i}{E_c}\times100\%
$$
100% modulation results in the peak of the lower sine wave and the trough of the upper sine wave to intersect. Over 100% modulation creates a flat area in the bounding sine waves, which is read in a receiver as audio distortion. 

If you are only given the sine wave(s), and not the equations or the values of $E_i$ and $E_c$, you can also calculate the value of the modulation index as:
$$
m=\frac{V_{peak}-V_{valley}}{V_{peak}+V_{valley}}
$$
Given the modulation index, you can also think of the function $E_{AM}$ as the following:
$$
E_{AM}(t)=(1+mE_i(t))(E_c\sin(\omega_ct+\theta))
$$


## Sidebands and Meaning

The equation shown above for the transmitted signal (both the carrier and the information) can be manipulated in the following manner:
$$
E_{AM}(t)=(E_c+E_i\sin(2\pi f_mt))\sin(\omega_ct+\theta)\\
E_{AM}(t)=E_c\sin(\omega_ct+\theta)+E_i\sin(2\pi f_mt)\sin(\omega_ct+\theta)
$$
The first term in this equation is the carrier term.

Further examination of the second term leads to the following discovery:
$$
E_i\sin(2\pi f_mt+\theta)\sin(2\pi f_ct+\theta)\\
\frac {E_i}2\cos(2\pi(f_c-f_m)t+\theta)-\frac {E_i}2\cos(2\pi(f_c+f_m)t+\theta)
$$
These two terms can be interpreted as two sinusoidal waveforms at frequencies $f_c-f_m$ and $f_c+f_m$, both with amplitude $\frac{E_i}2$. Plotting these on a voltage vs frequency bar graph leads to 3 peaks, as shown below:

![image-20201030212856948](lecture13.assets/image-20201030212856948.png)

Multiple sinusoids result in multiple peaks of varying heights, depending on their amplitudes in the function, but generally tending downwards as you separate from the carrier frequency.

Non-sinusoids can also be put through, still creating a "mirrored" lower and upper sideband, but at less regular intervals.



## Power

The following are accepted equations. They have been derived in the slides, but will not need to be derived, for sake of time:
$$
P_c=\frac{E_c^2}2\\
P_{usb}=\frac{m^2E_c^2}8=P_{lsb}\\
P_t=\frac{E_c^2(2+m^2)}4\\
\frac{P_i}{P_t}=\frac{m^2}{2+m^2}
$$
This last equation means that with a simple AM transmission, only 33% of the total power at absolute best can be used for actual data transmission. This efficiency can be improved in several methods.

One rather obvious way to improve efficiency is to only transmit the sidebands. This is called Double Sideband Suppressed Carrier, or DSB-SC.

to further increase efficiency, only one sideband can be transmitted. This can be done because both sidebands hold the same amount of information, so there is no information lost.

AM is also excellent for transmitting data, because it can essentially be "slowed down" and read by a device as normal. 

## Hardware Modulators

Modulators need to have a multiplier. This must be made for a non-linear device. A diode is an example of this, and generally is the core device that is used for modulators.

There are two forms of modulators, low level and high level. Low level modulators modulate at a low power level, and need to be amplified before transmitting. High level modulators amplify the carrier to a high power level.

### Low Level Modulator

![image-20201030225628787](Modulation.assets/image-20201030225628787.png)

This can also be implemented in a differential amplifier, in the following design:

![image-20201030230239303](Modulation.assets/image-20201030230239303.png)

### High level Modulator

![image-20201030230342643](Modulation.assets/image-20201030230342643.png)



There are also mixers built into singular chips. 

# Amplitude Demodulation

There are two forms of demodulators. Product detectors, and envelope detectors. Product detectors multiply the AM signal back down to the audio spectrum. Envelope detectors remove the information on the envelope of the signal. 

## Product Detector

Multiply all terms by a sinusoid. Fairly simple.

If the frequency is slightly off, the audio becomes distorted.

## Envelope Detector

The most common method is to use a peak detector. This can be done either with a circuit containing a diode, a capacitor, and a resistor, or a circuit like the one shown below. 

![image-20201030233430338](Modulation.assets/image-20201030233430338.png)

Putting a low pass filter at the output of the above filter will give you the original signal. 

## Comparison

| Product Detection                                      | Envelope Detection                                        |
| ------------------------------------------------------ | --------------------------------------------------------- |
| Mixes signal down to audio freq.                       | Simple method; easy to implement                          |
| Frequency alignment is critical for minimal distortion | Frequency alignment irrelevant                            |
| More complex than envelope detection                   | Creates more distortion for higher modulation percentages |

# Frequency and Phase Modulation

As with Amplitude Modulation, we start with the carrier signal ($V_c\sin(\omega_ct+\theta)$). We then change the angle of the carrier signal ($\omega_ct+\theta$) over time. 

Either the frequency or the phase changes, but not both. That being said, changing only the phase can result in either frequency or phase, and this is also true for the frequency. This is because frequency is the time rate of change of phase. 

Here is a brief comparison of the two methods. Both start from the general equation below:
$$
V(t)=V_c\sin(\omega_ct+\theta_m(t))
$$


|                                         | Phase Mod.                         | Freq. Mod.                                                 |
| --------------------------------------- | ---------------------------------- | ---------------------------------------------------------- |
| Phase term $\theta_m(t)$                | $\phi_dV_m(t)$                     | $2\pi f_d\displaystyle\int^t_{\infin}V_m(\lambda)d\lambda$ |
| Single tone mod. $V_c\sin(\omega_ct+x)$ | $x=\phi_dV_m\cos(2\pi f_mt)$       | $x=m_fV_m\sin(\omega_mt)$                                  |
| Mod. index                              | $\phi_d$                           | $m_f=\frac{f_d}{f_m}$                                      |
| Natural Generation                      | phase modulator                    | freq. modulator                                            |
| Secondary Generation                    | freq. modulator (derivative input) | phase modulator (integral input)                           |

The spectrum of these signals is complex. It is defined as a series of Bessel functions, which are shown in the slides, but we will not be using them for sake of time. The diagrams below, however, will be necessary.

![image-20201031000025532](Modulation.assets/image-20201031000025532.png)

![image-20201031000033503](Modulation.assets/image-20201031000033503.png)

## Bandwidth

The Bessel functions can be used to estimate the bandwidth. A second, easier way to find the approximate bandwidth is:
$$
BW=2(f_d+f_m)\\
f_d=max.\ peak\ freq.\ deviation\\
f_m=mod.\ freq\ or\ max\ freq
$$

## Hardware Modulators

### FM Modulators

Its inherently difficult to have an object with a variable frequency. Its essentially impossible to do so with an inductor. A specific form of diode however, called a varactor, is designed for this purpose, and acts like a variable capacitor. You can then use the variable capacitance in a resonant tank circuit with an amplifier, as shown below, to have a frequency modulator.

![image-20201031001349116](Modulation.assets/image-20201031001349116.png)

![image-20201031001553645](Modulation.assets/image-20201031001553645.png)

All this being said, LC oscillators are very unstable. Slightly different bias conditions, proximity to other materials, and even temperature differences can cause drifts in the "calibration" of the circuit.

The alternative is to use a crystal oscillator (often made of Quartz). This is more stable, because it works off of the natural properties of the crystal, which means it can be used more easily. However, because it is stable, that also means that there is a smaller frequency range when using a crystal in an oscillator or modulator. A varactor can be placed in AC series with the crystal to modify its resonant frequency, but only by a small margin. This means that using a crystal oscillator requires multiple amplifiers to bring the frequency up to the desired transmission frequency range. These are now often integrated into a simple device, called a VXO, or VCXO. (Voltage Controlled Crystal Oscillator), and as the name implies, their frequencies are modified by the input voltage, defined by the user.

### Phase Modulators

These are superior in some cases, due to the circuits ability to be stabilized outside of the modulation process. 

# Overview Comparison

|                           | Amplitude Modulation | Angle Modulation (Freq. or Phase) |
| ------------------------- | -------------------- | --------------------------------- |
| Amplitude?                | Varied               | Fixed                             |
| Frequency / Phase?        | Fixed                | Varied                            |
| BW?                       | Narrow               | Wide                              |
| Resilience to Noise?      | Low                  | High                              |
| BW/Info rate relationship | Simple               | Complex                           |



# Quadrature or Vector Modulation

This is a form of demodulation that can be used on either amplitude or frequency modulation. 