# Filters in Communications Systems

Where are they used?

Filters in transmitters are so you don't get in trouble with the FCC

filters in recievers are to remove blank information



Passive filters

- RC and LC filters
- crystal filters

active filters

- surface acoustic wave filters (saw)
- active filters (op-amps, resistors, and capacitors)
- digital filters



In a reciever, there's a band pass filter (usually LC SAW or resonator filters), then IF filters (intermediate frequency, usually LC, SAW, Crystal, and digital filters). On the transmission side, there's a harmonic filter (usually LC or resonator filters), a band pass filter (LC, SAW, Crystal filters)

# Passive filters

LC is good for low frequency, high bandwidth, SAW is good for high frequency high bandwidth, crystal is good for low frequency, narrow bandwidth



![image-20200902134423460](lecture5.assets/image-20200902134423460.png)

Simple RC filter



You'll need to know how to convert from L and C to X and Z

Frequency response of an RC filter, given $1k\Omega$ resistor and $0.033\mu F$ cap in series
$$
A=\frac{Z_c}{Z_{total}}\\
A=\frac{\frac{1}{sC}}{R+\frac 1{sC}}\\
A=\frac{\frac{1}{RC}}{s+\frac 1{RC}}\\
\omega=2\pi f_c=\frac1{RC}\\
\omega=2\pi f_c=\frac1{RC}\\
f_c=\frac1{2\pi RC}\\
$$
To compute the response at any individual frequency, $A_v=\frac{\omega_c}{s+\omega_c}=\frac{\omega_c}{j\omega+\omega_c}$

at the corner frequency, $j\omega=j\omega_c$

Corner frequency gain is -3dB, always. This breaks down to -20dB/decade, or -6dB/octave

phase angle of an RC filter will always be -45deg at the corner frequency. 

For every cap, its another -20dB/decade, or -6dB/octave

Throwing two RC filters right next to each other brings the corner frequency in, and we don't want that. So, we can throw an op amp between them to be a buffer between the two stages. It helps, but its not perfect.



# Active RC Filters

## Sallen-Key Filters

design multi-pole RC filters using op amps

remember the butterworth filter

Pick $f_c$ and $C_1$. $C_2=2C_1;\ R_1=R_2={1\over\sqrt2\times2C_1f_c}$

This filter is active, but its corner frequency is exactly where you want it to be

....well, it curves back with super high frequencies, so be careful