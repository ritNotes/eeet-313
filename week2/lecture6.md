Note for the quiz, make sure that you look at the units, not the letters

# Parallel resonant circuits

Find Q, $f_{resonant}$ 

We've talked RL and RC active and passive circuits

This only really works with low frequencies. Op-Amps can't really deal with that stuff



RLC network ($R+(L||C)$)

This is a band pass

resonant frequency of RLC is when $\frac1{X_L}=B_L=B_C=\frac1{X_C}$

$f_r=\frac1{2\pi\sqrt{LC}}$



This parallel LC is also called a tank circuit



## In class problem

1V, $R=50\Omega, L=0.05\mu H, C=25pF$

$\approx142MHz$





$Q=\frac RX$

Q is calculated at the resonant frequency

If you want more selectivity, we want a higher Q. If we want a wider frequency response, we want a lower Q.

$BW=\frac {f_r}{Q}$



Source and load will have resistance. These need to be considered.

Common values for $50\Omega, 75\Omega,300\Omega$

50 for RF circuit, 75 for cabling, 300 for antennas (generally)

Source and load resistance are in parallel when computing Q. This becomes the "Loaded Q"

 Adjust the Q by changing the inductor. Re-center the frequency by adjusting the capacitor



Low Q network is a soft curve. High Q network looks like a point, by comparison



Components also have a Q value. Inductors (which have a series resistor) can calculate a Q as $X_L\over R_s$

$R_{parallel}=(Q^2+1)R_{series}$

$X_{parallel}={R_{series}\over Q}$



Caps have a parallel resistor 



We will generally model an inductor with a resistor, with a Q value, and a cap that is "ideal".... for now



Adding the Q resistor, you find that the peak of the graph dips