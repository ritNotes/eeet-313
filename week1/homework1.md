# Skyler MacDougall

## EEET-313-01S2: Communications Electronics

### Homework 1

1. Calculate the wavelength (in meters) of a signal in free space with a frequency of 350MHz.
    $$
    c=\lambda f\\
    228792458m/s=\lambda (350*10^6Hz)\\
    \overline{\underline{|\lambda=0.856m|}}
    $$

2. A quarter wavelength antenna is often used as a vertical antenna mounted on the ground.  The antenna is one-quarter wavelength long and the signal is reflected from the earth ground.  Compute the length (in meters) of a quarter wavelength antenna for a frequency of 146 MHz.
    $$
    c=\lambda f\\
    c=\lambda (146*10^6Hz)\\
    \lambda=2.05m\\
    \overline{\underline{|\frac \lambda 4=0.513m|}}
    $$

3. A half-wave dipole is an antenna that is mounted between 2 objects (towers, trees, etc.) and is fed with a signnal in the center. The length of the antenna is 1/2 of the wavelength of the signal frequency. What is the length of a half wavelength dipole antenna (in meters) for a frequency of 7.1MHz which is in the Ham Radio band.
    $$
    c=\lambda f\\
    c=\lambda (7.1*10^6Hz)\\
    \lambda=42.22m\\
    \overline{\underline{|\frac \lambda 2=21.11m|}}
    $$

4. My neighbor has installed an end-fed wire antenna from his attic to a nearby tree. The length of the wire looks to be about 70 feet long. I know that an end-fed antenna is typically about 1/4 wavelength long. Estimate the frequency at which my neighbor is transmitting or receiving. Recall that the speed of light in free space is 186282 miles per second.
    $$
    c=\lambda f\\
    c=(70ft*4)f\\
    \overline{\underline{|f=3.51MHz|}}
    $$

5. Near Field Communications (NFC) is a technology that is used in cellphone and other devices to communicate over very short ranges (~4cm). It can be used to make credit card and banking transactions at Wegmans or other retail stores and many other applications. If the wavelength in free space of the signal that is used is 22.12 meters, what is the frequency used for NFC.
    $$
    c=\lambda f\\
    c=(22.12m)f\\
    \overline{\underline{|c=13.55MHz|}}
    $$

6. I want to transmit a signal from my radio station in Rochester NY to my father who lives in Western Massachusetts, a distance of 230 miles. How long will it take my transmission to get to my father after I transmit the signal. Recall that the propagation time is.
    $$
    v=\frac d t\\
    c=\frac {230\ miles} t\\
    \overline{\underline{|t=1.234ms|}}
    $$
    

7. NASA can send control signals to the Mars rover using radio waves and the Mars rover transmits telemetry signals from Mars back to Earth. What is the delay in minutes from the time that the Mars rover transmits and the signal is received on earth? Assume free space transmission and the average distance from Mars to the Earth is 140 million miles.
    $$
    v=\frac d t\\
    c=\frac {140*10^6\ miles} t\\
    t=751.54\ seconds\\
    \overline{\underline{|t=12 minutes,\ 31.54\ seconds|}}
    $$
    

8. In an RG213 coaxial cable, the velocity or propagation of a signal is slower than that in free space. In RG213 coaxial cable it is 66% of the speed of light. How long does it take for a signal to travel from one end to the other in an RG213 coaxial cable that is 12 miles long.
    $$
    c=\frac \lambda t\\
    c*0.66=\frac {(12\ miles)} t \\
    t=97.6\mu s
    $$
    

9. A narrow-band communications signal used for public safety communications to communicate audio has a spectrum that looks like the figure below. The following is a list of the frequencies shown in the diagram. The regions indicate the 50% total power, 90% total power, and the 99% total power bandwidths. 
    ![image-20200821204437028](homework1.assets/image-20200821204437028.png)

    - F1=459.975MHz
    - F2=459.990MHz
    - F3=459.995MHz
    - F4=460.005MHz
    - F5=460.010MHz
    - F6=460.025MHz

    What are the 50%, 90%, and 99% bandwidths of the signal?
    $$
    50\%\ BW=f_{upper}-f_{lower}\\
    50\%\ BW=460.005MHz-459.995MHz\\
    \overline{\underline{|50\%\ BW=10kHz|}}\\
    90\%\ BW=f_{upper}-f_{lower}\\
    90\%\ BW=459.990MHz-460.010MHz\\
    \overline{\underline{|90\%\ BW=20kHz|}}\\
    99\%\ BW=f_{upper}-f_{lower}\\
    99\%\ BW=459.975MHz-460.025MHz\\
    \overline{\underline{|99\%\ BW=50kHz|}}\\
    $$

10. A 5 GHz WiFi communications signal has a spectrum that looks like the figure in question 9 and is centered at 5280MHz. The following is a list of the approximate frequencies shown in the diagram. The regions indicate the 50% total power, 90% total power and the 99% total power bandwidths.

    - F1=5264MHz
    - F2=5269MHz
    - F3=5271MHz
    - F4=5289MHz
    - F5=5291MHz
    - F6=5296MHz

    What are the 50%, 90%, and 99% bandwidths of the signal?
    $$
    50\%\ BW=f_{upper}-f_{lower}\\
    50\%\ BW=5289MHz-5271MHz\\
    \overline{\underline{|50\%\ BW=18MHz|}}\\
    90\%\ BW=f_{upper}-f_{lower}\\
    90\%\ BW=5291MHz-5269MHz\\
    \overline{\underline{|90\%\ BW=22MHz|}}\\
    99\%\ BW=f_{upper}-f_{lower}\\
    99\%\ BW=5296MHz-5264MHz\\
    \overline{\underline{|99\%\ BW=32MHz|}}\\
    $$

11. A low pass filter has the shape shown below. What are the 3dB, 20dB, and 40dB bandwidths?
     ![image-20200821210401983](homework1.assets/image-20200821210401983.png)
     $$
     3dB\ BW=50MHz\\
     20dB\ BW=500MHz\\
     40dB\ BW=5GHz
     $$

12. A front end filter for a public safety band radio operating in the UHF band has a frequency response that looks like the figure below. What is the 3dB BW of the filter? What are the 20dB and 60dB bandwidths?
     ![image-20200821210837285](homework1.assets/image-20200821210837285.png)
     $$
     3dB\ BW=20MHz\\
     20dB\ BW=40MHz\\
     40dB\ BW=60MHz
     $$
     

