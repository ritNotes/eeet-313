Carrier signals good

carrier signals have two waves, a "real" and an "imaginary"

imaginary is shifted by $90^\circ$ from the real



carrier CARRIES things. doesn't do anything by itself

can modify the magnitude (amplitude modulation or AM), the phase (phase or angle modulation), or the frequency (frequency modulation or FM)

can also modify multiple (common example is phase and amplitude, or QAM modulation)

general equation for carrier wave: $v(t)=V_p(t)cos(\theta t)+jV_p(t)sin(\theta t)$

this equation is conceptual.

start here:

$v_r(t)=V_p(t)cos(\theta(t))$

this can be broken down into the following:

$v_r(t)=V_p(t)cos(2\omega t+\phi)=V_p(t)cos(2\pi ft+\phi)$

where

$\omega$ is angular velocity (in radians per second)

$f$ is the frequency (in cycles per second)

$\phi$ is the fixed phase term



amplitude, frequency, and phase are the **only** things we can change. the equation may look complex, but thats it



we will then apply the wave to an antenna, and that will create the wave that we will use to pass along information

current creates a magnetic field, a voltage creates the electric field. these fields push waves out in regular patterns



the bandwidth of a signal is how much of a spectrum it uses. but, cause its a bell curve, its gotta be a percentage of the total energy. (generally 90 or 99%)

remember filters from before. they're named in reasonable ways, at least the easy ones. Filter bandwidth is the 3dB bandwidth. (so, the frequency where 3dB is lost) another common point is the 60dB bandwidth or the 20dB point

state what you mean by a bandwidth when you're talking with people





