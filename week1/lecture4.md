# Fourier series

$$
f(x)=\frac12 a_0+a_1cos(\omega_0t) +b_1sin (\omega_0t) +a_2 cos( \omega_0t) +b_2sin(\omega_0t)...\\
where\\
\frac12a_0=DC\\
a_1cos(\omega_0t)+b_1sin(\omega_0t)=fundamental\ frequency\\
a_2cos(\omega_0t)+b_2sin(\omega_0t)=2^{nd}\ harmonic\\
etc
$$



## How to calculate the coefficients:

integrate across one period of the signal
$$
a_0=\frac1{T_0}\int^{T_0}_0f(x)dx\\
a_n=\frac1{T_0}\int^{T_0}_0cos(nx)f(x)dx\\
b_n=\frac1{T_0}\int^{T_0}_0sin(nx)f(x)dx\\
$$
