# Gain, Decibels, and Loss

- Calculate decibel and numerical gain
- describe decibel, and compute
- State the gain and loss numerically



Systems have a large range of levels. Transmitters output 100W, while recievers use $10^{-15}W$. Within a tranciever, it can be micro to femto volts.

Decibel calculation:
$$
P_{dB}=10log_{10}(\frac {P_1} {P_2} )
$$
Voltage decibels use 20log instead of 10log.

Gain calculation:
$$
A=\frac {P_{out}}{P_{in}}
$$
gain can also be done in decibels

 

```
if (numerical gain > 1) gain=gain;
else gain=loss;
```



cascaded gain, multiply gains

cascaded decibel gain, add them



numerical gain > 1 == decibel gain > 0

numerical gain < 1 == decibel gain < 0



dBmV

dBuV