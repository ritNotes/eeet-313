
$$
4.14MHz\\
\frac1{2\pi f_rC}=\frac1{2\pi (4.14MHz)(82pF)}=468\Omega\\
{2k\Omega\over468\Omega}
$$
