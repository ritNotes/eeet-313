# Butterworth

![image-20200911135151016](Untitled.assets/image-20200911135151016.png)

![image-20200911135331768](Untitled.assets/image-20200911135331768.png)

# Chebyshev

![image-20200911135759623](Untitled.assets/image-20200911135759623.png)

![image-20200911135215203](Untitled.assets/image-20200911135215203.png)

![image-20200911135234644](Untitled.assets/image-20200911135234644.png)

# Ex.1

Build a 3 pole dB chebyshev with $f_c=155MHz$ and $R_s=R_l=50\Omega$
$$
C_1=\frac1{2\pi f_cR_0}c_1;\ L_2=\frac{R_0}{2\pi f_c}l_2;\ etc\\
$$



