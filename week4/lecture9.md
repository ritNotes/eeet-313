Maximum power:

For a source ($V_S$, with $R_S$ and $X_S$ in series after that), available power is $V_S^2\over4R_S$

Whats the maximum power transfer?

Set $R_S=R_L$



What is the available power from the following source:
$$
V_s=V_{p-p}\\
R_s=75\Omega\\
X_s=j300\Omega
$$
You need to add a $75-j300\Omega$ load, first.

Then:

Available power:
$$
V_L=V_S(\frac{R_L}{R_s+R_L})=\frac {V_S} 2\\
P_{av}=\frac{V_{S_{RMS}}^2}{4R_S}\\
P_{av}=1.7mW
$$
What if the power is delivered to a $300\Omega$ load?

$$
Delivered\ power\\
P_L=V_S^2(\frac{R_L}{(R_S+R_L)^2})
$$


Always make sure that your power is less than or equal to your max power



What if the resistances aren't equal?

# L-Matching

$$
Q=\sqrt{\frac{R_{large}}{R_{small}}-1}\\
X_{series}=Q*R_{small}\\
X_{shunt}=X_{series}(1+\frac1{Q^2})
$$

