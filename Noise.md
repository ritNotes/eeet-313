# Noise

There are 3 types of noise external to a circuit:

- Atmospheric
    - static electricity
- Extra-Terrestrial Noise
    - Solar flares
- Man-Made Noise
    - Motors, generators, fluorescent lights, etc

There's also internal noise:

- Thermal noise
- Shot noise
    - Random arrival of carriers in a semiconductor due to quantum tunneling
- Transmit Time Noise
    - caused by carrier movement in a semiconductor over a long period of time compared to a cycle

All resistors create thermal noise, often called "white noise". White noise contains all frequencies, at random amplitudes. Filtered noise is "pink noise" because... ???

The voltage noise in a resistor is calculated as follows:
$$
V_n=\sqrt{4kTBR_s}\\
k=Boltzmann's\ constant=1.38\times10^{-23}J/K\\
T=temperature(K)\\
B=BW (Hz)\\
R=Resistance(\Omega)
$$
Under ideal conditions, the power delivered due to noise is $P_l=kTB$

Remember for decibel power calculations:
$$
dB_m=10\log_{10}(\frac{P}{1mW})\\
$$


All of these noise calculations are meaningless without context.

Given an antenna, and a $50\Omega$ receiver, over a bandwidth of $1Hz$. The noise from the receiver is $-174dB_m$. A typical low level received on the antenna might be $-120dB_m$.
This is where the ratio of signal to noise is used. It is usually expressed in decibels, and can also be noted as "SNR". There are multiple different forms, including but not limited to:

- $\frac SN$
    - Signal to Noise ratio
    - $N$= the noise power in a specified bandwidth
    - used in analog systems
    - function of the measurement bandwidth
    - can only compare with the same bandwidth
- $\frac C{N_0}$
    - Carrier to Noise density ratio
    - $N_0$=the noise power in a $1Hz$ bandwidth.
    - used in communications systems
    - easy to compare, independent of bandwidth
- $\frac{E_b}{N_0}$
    - energy per bit to noise density ratio
    - independent of bandwidth or bitrate
    - allows comparison of different data signaling formats

A receiver's sensitivity is based off this value, and is defined as a certain level of quality at the output of the demodulator, given the noise of the system.

Another important value accounts for the amount of distortion in *audio* quality. Distortion refers to harmonics. This also called SINAD, refering to Signal, Noise and Distortion. Its defined as the following:
$$
SINAD=10\log_{10}(\frac{S+N+D}{N+D})
$$
SNR degrades as it progresses through a system, and each stage has some level of noise contribution. As such, putting the noisiest components at the beginning is suggested to reduce noise as much as possible.