# Carrier Signal

- ideal sinusoidal
- ideally single frequency

in order to create the carrier signal, we need a feedback circuit. 

Transfer function of the feedback system: $\frac{A_v}{1-A_v\beta(j\omega)}$



The Barkhausen Criteria:

The loop will have a sustained oscillation at frequencies where:

- $|A_v\beta(j\omega)|\ge1$
- $\ang [A_v\beta(j\omega)]=n\times2\pi$



Time to build an oscillator.

We need:

- A gain stage
- a frequency selective feedback loop that has the right phase shift around the loop

Lets start with an op-amp. Just that.

![image-20200930134925330](Untitled.assets/image-20200930134925330.png)

We need an additional $180^\circ$ phase shift to oscillate. 

If we're using an RC filter, the corner frequency phase shift is $45^\circ$, so we need 4. They all need the same values, so multiply each resistor by 10 successively, and the capacitor by 10 consistently. ($C=\frac1{2\pi f_cR}$)

![image-20200930135510430](Untitled.assets/image-20200930135510430.png)



Now, this signal is great. But it doesn't work well in simulations, so the `+5V_pulse` is to start the transient response, to make it "lifelike".

An op amp is kinda bad for high frequency shit. So, we'll use an LC circuit for the phase shift, and a transistor for the gain block.

![image-20200930140607122](Untitled.assets/image-20200930140607122.png)

Reminder:
$$
f_c=\frac1{2\pi \sqrt{L_{total}C_{total}}}
$$
Also remember that capacitors in series add as resistors do in parallel $\frac1{R_{total}}=\frac1{R_1}+\frac1{R_2}+...$ and inductors in series add as resistors do in series $R_{total}=R_1+R_2+...$

![image-20200930141559458](Untitled.assets/image-20200930141559458.png)

REMEMBER

Transistors have output and input capacitance. You need to compensate for this in the system. If your resonant components are an order of magnitude higher, then you will be fine, mostly


